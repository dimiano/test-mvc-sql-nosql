﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataModel
{
    public class User
    {
        public User(string name)
        {
            Name = name;
            Created = DateTime.UtcNow;
        }

        [Key]
        public int Id { get; private set; }
        [Required]
        //EF doesn't support Unique Constraint
        [MaxLength(100, ErrorMessage = "User Name must be 100 characters or less"), MinLength(3)]
        public string Name { get; set; }
        [Required]
        public DateTime Created { get; set; }        
        //public virtual IList<SomeItem> SomeItems { get; set; }
        [InverseProperty("CreatedBy")]
        public virtual IList<SomeItem> ItemsCreated { get; set; }
        [InverseProperty("UpdatedBy")]
        public virtual IList<SomeItem> ItemsUpdated { get; set; }
    }
}
