﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;

namespace DataModel
{
    public class SomeContext : DbContext
    {
        public SomeContext()
            : base("SomeConnectionString")
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<SomeItem> SomeItems { get; set; }
    }
}
