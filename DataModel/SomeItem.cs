﻿using System.ComponentModel.DataAnnotations;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataModel
{
    public class SomeItem
    {
        public SomeItem()
        {
        }

        public SomeItem(User user)
        {
            CreatedBy = user;
            UpdatedBy = user;
        }

        [Key]
        public int Id { get; private set; }
        [Required]
        public string Title { get; set; }
        public string Description { get; set; }
        [Timestamp]
        public byte[] TimeStamp { get; set; }
        public User CreatedBy { get; set; }
        public User UpdatedBy { get; set; }
        [NotMapped]
        public byte LockMode { get; set; }
    }
}