﻿using System;

namespace TestConsole
{
    internal static class FirstTest
    {
        private class Cl
        {
            public int i;

            public Cl()
            {
                i = 0;
            }
        }

        private struct Str
        {
            public int i;
        }

        private static void f1(Cl obCl)
        {
            obCl.i += 10;
            var obCl2 = new Cl();
            obCl2.i = obCl.i;
            obCl2.i += 100;
            obCl = obCl2;
        }

        private static void f2(ref Cl obCl)
        {
            obCl.i += 1000;
            var obCl2 = new Cl();
            obCl2.i = obCl.i;
            obCl2.i += 10000;
            obCl = obCl2;
        }

        private static void fs(Str obStr)
        {
            obStr.i += 20;
            Str obStr2;
            obStr2.i = obStr.i;
            obStr2.i += 200;
            obStr = obStr2;
        }

        private static void fs2(ref Str obStr)
        {
            obStr.i += 200;
            Str obStr2;
            obStr2.i = obStr.i;
            obStr2.i += 2000;
            obStr = obStr2;
        }

        internal static void DoTest()
        {
            var obCl = new Cl();
            obCl.i += 1;
            f1(obCl);
            f2(ref obCl);

            Str obStr;
            obStr.i = obCl.i;
            fs(obStr);
            fs2(ref obStr);

            Console.WriteLine("obStr.i = {0}\nobCl.i  = {1}\n", obStr.i, obCl.i);
            Console.ReadKey();
        }
    }
}
