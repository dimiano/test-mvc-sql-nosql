﻿using System;
using System.Data.SqlClient;
using System.Linq;
using SomeMvcApp.Models;

namespace TestConsole
{
    internal static class TestContext
    {
        internal static void DoTest()
        {
            using (var db = new SomeContext())
            {
                try
                {
                    db.Database.Delete();
                    db.SaveChanges();
                }
                catch (SqlException ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine("\n\nPress any key to exit...");
                    Console.ReadKey();
                }

                var user = new User("TestUser");
                db.Users.Add(user);

                db.SomeItems.Add(new SomeItem(user) { Title = "Test item 0" });
                db.SomeItems.Add(new SomeItem(user) { Title = "Test item 1" });
                db.SomeItems.Add(new SomeItem(user) { Title = "Test item 2" });
                db.SaveChanges();

                // Display all items from the database
                var query = from b in db.SomeItems
                            orderby b.Title
                            select b;

                Console.WriteLine("\nAll items in the database:");
                foreach (var item in query)
                {
                    Console.WriteLine("ID:{0}, \"{1}\", '{2}'", item.Id, item.Title, item.UpdatedBy.Name);
                }

                var itemToDelete = query.First();
                Console.WriteLine("\nDeleted item ID:{0}, Title:\"{1}\"", itemToDelete.Id, itemToDelete.Title);

                db.SomeItems.Remove(itemToDelete);
                db.SaveChanges();

                //query = from b in db.SomeItems
                //        orderby b.Title
                //        select b;

                Console.WriteLine("\nAll items in the database after deliting:");
                foreach (var item in query)
                {
                    Console.WriteLine("ID:{0}, \"{1}\", '{2}'", item.Id, item.Title, item.UpdatedBy.Name);
                }

                var itemToUpdate = query.OrderByDescending(s => s.Title).First();
                itemToUpdate.Title = "Test item Updated";
                var user2 = new User("TestUser 2");
                itemToUpdate.UpdatedBy = user2;
                Console.WriteLine("\nUpdated item ID:{0}, Title:\"{1}\"", itemToUpdate.Id, itemToUpdate.Title);

                db.Users.Add(user2);
                db.SaveChanges();

                Console.WriteLine("\nAll items in the database after updating:");
                foreach (var item in query)
                {
                    Console.WriteLine("ID:{0}, \"{1}\", '{2}'", item.Id, item.Title, item.UpdatedBy.Name);
                }

                Console.WriteLine("\n\nPress any key to exit...");
                Console.ReadKey();
            }
        }
    }
}
