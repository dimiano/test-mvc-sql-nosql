﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using Raven.Abstractions.Extensions;
using Raven.Client.Document;
using Raven.Client.Embedded;
using Raven.Client.Linq;
using SomeMvcApp.Models;
using WebMatrix.WebData;

namespace SomeMvcApp
{
	public class TestStore
	{
		 #region Singleton

		private static volatile TestStore _instance;

		public DocumentStore Store { get; set; }

		private TestStore()
		{
		}

		public static TestStore Instance
		{
			get
			{
				if (_instance == null)
				{

					if (_instance == null)
						_instance = new TestStore();
				}

				return _instance;
			}
		}
		#endregion
	}

	public class MvcApplication : System.Web.HttpApplication
	{
		// ReSharper disable InconsistentNaming
		protected void Application_Start()
		{
		// ReSharper restore InconsistentNaming
			// Run DB initializer with test data in debug config only
			//Database.SetInitializer<SomeContext>(Debugger.IsAttached ? new SomeContextInitializer() : null);
			//InitDocStore(); 
			SeedDocStore();

			AreaRegistration.RegisterAllAreas();

			WebApiConfig.Register(GlobalConfiguration.Configuration);
			FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
			RouteConfig.RegisterRoutes(RouteTable.Routes);
			BundleConfig.RegisterBundles(BundleTable.Bundles);
			ValueProviderFactories.Factories.Add(new JsonValueProviderFactory());
		}

		private static void InitDocStore()
		{
			TestStore.Instance.Store = new EmbeddableDocumentStore { ConnectionStringName = "RavenEmbedded" };
			TestStore.Instance.Store.Initialize();
			ClearStore();
		}

		private static void ClearStore()
		{
			using (var session = TestStore.Instance.Store.OpenSession())
			{
				var items = session.Query<SomeItem>().Select(it => it).AsEnumerable();
				items.ForEach(session.Delete);
				session.SaveChanges();
				//session.Advanced.DocumentStore.DatabaseCommands.Delete(someItem.Id.ToString(),null);
			}
		}

		private static void SeedDocStore()
		{
			InitDocStore();
			var session = TestStore.Instance.Store.OpenSession();

			var user1 = new User("User1");
			var user2 = new User("User2");

			// WebSecurity.CreateUserAndAccount will add users
			session.Store(user1, "users/1");
			session.Store(user2, "users/2");
			session.SaveChanges();

			RegisterUser(user1);
			RegisterUser(user2);

			var someItems = new List<SomeItem>
									 {
										 new SomeItem(user1) {Title = "Test item 1", Description = "Some test description"},
										 new SomeItem(user1) {Title = "Test item 2", Locked = true},
										 new SomeItem(user1) {Title = "Test item 3", Description = "Description three"},
										 new SomeItem(user2) {Title = "Test item 4"},
										 new SomeItem(user1) {Title = "Test item 5", Locked = true, UpdatedBy = user2},
										 new SomeItem(user2) {Title = "Test item 6", Locked = true},
										 new SomeItem(user2) {Title = "Test item 7", UpdatedBy = user1}
									 };
			someItems.ForEach(session.Store);
			session.SaveChanges();
		}

		private static void RegisterUser(User user)
		{
			try
			{
				if (!WebSecurity.Initialized)
				{
					WebSecurity.InitializeDatabaseConnection("SomeConnection", "Users", "Id", "Name", true);
				}

				//WebSecurity.CreateUserAndAccount(user.Name, "123456", new { Created = user.Created });
				WebSecurity.CreateAccount(user.Name, "123456");
			}
			catch (MembershipCreateUserException)
			{
				/*The username is already in use.*/
			}
		}
	}
}