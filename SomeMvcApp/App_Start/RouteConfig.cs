﻿using System.Web.Mvc;
using System.Web.Routing;

namespace SomeMvcApp
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional}
            );

            routes.MapRoute(
                name: "EditLocked",
                url: "{controller}/{action}/{id}/{isLocked}",
                defaults: new { controller = "Some", action = "Edit", id = UrlParameter.Optional, isLocked = UrlParameter.Optional}
            );
        }
    }
}