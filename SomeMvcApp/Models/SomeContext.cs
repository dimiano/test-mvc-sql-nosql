﻿using System.Collections.Generic;
using System.Data.Entity;
using WebMatrix.WebData;

namespace SomeMvcApp.Models
{
    public class SomeContext : DbContext
    {
        public SomeContext()
            : base("SomeConnection")
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<SomeItem> SomeItems { get; set; }
    }

    internal class SomeContextInitializer : DropCreateDatabaseIfModelChanges<SomeContext>
    {
        protected override void Seed(SomeContext context)
        {
            var user1 = new User("User1");
            var user2 = new User("User2");

            // WebSecurity.CreateUserAndAccount will add users
            context.Users.Add(user1);
            context.Users.Add(user2);
            context.SaveChanges();
            
            RegisterUser(user1);
            RegisterUser(user2);

            var someItems = new List<SomeItem>
                                     {
                                         new SomeItem(user1) {Title = "Test item 1", Description = "Some test description"},
                                         new SomeItem(user1) {Title = "Test item 2", Locked = true},
                                         new SomeItem(user1) {Title = "Test item 3", Description = "Description three"},
                                         new SomeItem(user2) {Title = "Test item 4"},
                                         new SomeItem(user1) {Title = "Test item 5", Locked = true, UpdatedBy = user2},
                                         new SomeItem(user2) {Title = "Test item 6", Locked = true},
                                         new SomeItem(user2) {Title = "Test item 7", UpdatedBy = user1}
                                     };
            someItems.ForEach(si => context.SomeItems.Add(si));
            context.SaveChanges();
            base.Seed(context);
        }

        private static void RegisterUser(User user)
        {
            if (!WebSecurity.Initialized)
            {
                WebSecurity.InitializeDatabaseConnection("SomeConnection", "Users", "Id", "Name", true);
            }
            
            //WebSecurity.CreateUserAndAccount(user.Name, "123456", new { Created = user.Created });
            WebSecurity.CreateAccount(user.Name, "123456");
        }
    }
}
