﻿using System;
using System.Collections.Generic;

namespace SomeMvcApp.Models.Repository
{
	public interface IRepository : IDisposable
	{
		IEnumerable<SomeItem> GetItems(string sortOrder = null, string searchString = null);
		IEnumerable<User> GetUsers();
		SomeItem GetItemById(int someId);
		void InsertItem(SomeItem someItem, string userName);
		void DeleteItem(int someId);
		void DeleteItem(SomeItem someItem);
		void UpdateItem(SomeItem someItem);
		User GetUserByName(string userName);
		void LoadReference(SomeItem someItem);
		void Save();
		void UnlockAllUsersItems(string userName);
	}
}