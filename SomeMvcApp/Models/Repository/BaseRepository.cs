﻿using System.Collections.Generic;
using System.Linq;

namespace SomeMvcApp.Models.Repository
{
    internal abstract class BaseRepository
    {
        #region Helpers Methods

        protected IEnumerable<SomeItem> GetSortedItems(IEnumerable<SomeItem> items, string sortOrder = "")
        {
            if (items != null && items.Count() > 0)
            {
                if (!string.IsNullOrWhiteSpace(sortOrder))
                {
                    switch (sortOrder)
                    {
                        case "title desc":
                            items = items.OrderByDescending(s => s.Title);
                            break;
                        case "lock":
                            items = items.OrderBy(s => s.Locked);
                            break;
                        case "lock desc":
                            items = items.OrderByDescending(s => s.Locked);
                            break;
                        case "descript":
                            items = items.OrderBy(s => s.Description);
                            break;
                        case "descript desc":
                            items = items.OrderByDescending(s => s.Description);
                            break;
                        case "user":
                            items = items.OrderBy(s => s.CreatedBy.Name);
                            break;
                        case "user desc":
                            items = items.OrderByDescending(s => s.CreatedBy.Name);
                            break;
                        default:
                            items = items.OrderBy(s => s.Id);
                            break;
                    }
                }
            }

            return items;
        }

        protected IEnumerable<SomeItem> GetFilteredItems(IEnumerable<SomeItem> items, string searchString = "")
        {
            if (items != null && items.Count() > 0)
            {
                if (!string.IsNullOrWhiteSpace(searchString))
                {
                    items = items.Where(s => s.Title.ToUpper().Contains(searchString.ToUpper())
                                                   || s.Title.ToUpper().Contains(searchString.ToUpper()));
                }
            }

            return items;
        }

        #endregion
    }
}