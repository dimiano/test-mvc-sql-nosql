﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace SomeMvcApp.Models.Repository
{
	internal class SomeContextRepository : BaseRepository, IRepository
	{
		private readonly SomeContext _context;

		public SomeContextRepository(SomeContext context)
		{
			_context = context;
		}
		
		/// <summary>
		/// Load the User related to a given SomeItem
		/// </summary>
		/// <param name="someItem">SomeItem instance to load it's user info</param>
		public void LoadReference(SomeItem someItem)
		{
			_context.Entry(someItem).Reference(si => si.CreatedBy).Load();
			_context.Entry(someItem).Reference(si => si.UpdatedBy).Load();
		}

		#region Implementation of IRepository

		public IEnumerable<SomeItem> GetItems(string searchString, string sortOrder)
		{
			var items = _context.SomeItems.Include("UpdatedBy").Select(s => s); //TODO verify if "Select" needed
			//TODO use GetFilteredItems and GetSortedItems methods
			if (!string.IsNullOrWhiteSpace(searchString))
			{
				items = items.Where(s => s.Title.ToUpper().Contains(searchString.ToUpper())
									   || s.Title.ToUpper().Contains(searchString.ToUpper()));
			}

			if (!string.IsNullOrEmpty(sortOrder))
			{
				switch (sortOrder)
				{
					case "title desc":
						items = items.OrderByDescending(s => s.Title);
						break;
					case "lock":
						items = items.OrderBy(s => s.Locked);
						break;
					case "lock desc":
						items = items.OrderByDescending(s => s.Locked);
						break;
					case "descript":
						items = items.OrderBy(s => s.Description);
						break;
					case "descript desc":
						items = items.OrderByDescending(s => s.Description);
						break;
					case "user":
						items = items.OrderBy(s => s.CreatedBy.Name);
						break;
					case "user desc":
						items = items.OrderByDescending(s => s.CreatedBy.Name);
						break;
					//default:
					//    items = items.OrderBy(s => s.Id);
					//    break;
				} 
			}

			return items;
		}

		public IEnumerable<User> GetUsers()
		{
			return _context.Users.Include("ItemsCreated").Include("ItemsUpdated");
		}

		public SomeItem GetItemById(int someId)
		{
			SomeItem someitem = _context.SomeItems.Find(someId);
			
			if (someitem != null)
			{
				LoadReference(someitem); 
			}
			
			return someitem;
		}

		public void InsertItem(SomeItem someItem, string userName)
		{
			var currentUser = GetUserByName(userName);
			someItem.CreatedBy = currentUser;
			someItem.UpdatedBy = currentUser;

			_context.SomeItems.Add(someItem);
		}

		public void DeleteItem(int someId)
		{
			DeleteItem(_context.SomeItems.Find(someId));
		}

		public void DeleteItem(SomeItem someItem)
		{
			if (someItem != null)
			{
				//_context.SomeItems.Remove(someItem); // permanent remove without warning
				_context.Entry(someItem).State = EntityState.Deleted;
			}
		}

		public void UpdateItem(SomeItem someItem)
		{
			_context.Entry(someItem).State = EntityState.Modified;
		}
		
		/// <summary>
		/// Permanent Save
		/// </summary>
		/// <param name="someItem">Item to unlock and save</param>
		public void StrongUpdateItem(SomeItem someItem)
		{
			var updatedItem = _context.SomeItems.Find(someItem.Id);
			// end editing
			updatedItem.Locked = false;
			_context.Entry(updatedItem).CurrentValues.SetValues(someItem);
			_context.SaveChanges();
		}

		public User GetUserByName(string userName)
		{
			return _context.Users.Single(u => u.Name.ToLower() == userName.ToLower());
		}

		public void Save()
		{
			_context.SaveChanges();
		}

		/// <summary>
		/// Unlock all user's items if they are locked
		/// </summary>
		/// <param name="userName">User name</param>
		public void UnlockAllUsersItems(string userName)
		{
			var user = GetUserByName(userName);
			var lockedItemsList = user.ItemsUpdated.Where(it => it.Locked).ToList();
			lockedItemsList.ForEach(it => it.Locked = false);

			Save();
		}

		#endregion

		#region Implementation of IDisposable

		private bool _disposed;

		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
		/// </summary>
		/// <filterpriority>2</filterpriority>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (!_disposed)
			{
				if (disposing)
				{
					_context.Dispose();
				}
			}
			_disposed = true;
		}

		#endregion

	}
}