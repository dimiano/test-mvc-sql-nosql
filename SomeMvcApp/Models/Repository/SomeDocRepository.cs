﻿using System;
using System.Collections.Generic;
using System.Linq;
using Raven.Abstractions.Extensions;
using Raven.Client;
using Raven.Client.Document;

namespace SomeMvcApp.Models.Repository
{
	internal class SomeDocRepository : BaseRepository, IRepository
	{
		//private readonly IDocumentStore _store;
		private readonly IDocumentSession _session;

		public SomeDocRepository(DocumentStore documentStore) //EmbeddableDocumentStore
		{
			//_documentStore = new DocumentStore { ConnectionStringName = "RavenDB" }; // Url=http://localhost:8080
			//_store = documentStore; // DataDirectory = "path/to/database/directory"
			//_store.Initialize();
			//_session = _store.OpenSession();
			_session = TestStore.Instance.Store.OpenSession();
		}

		#region Implementation of IRepository

		public IEnumerable<SomeItem> GetItems(string sortOrder, string searchString)
		{
			var items = _session.Query<SomeItem>().Include(si => si.UpdatedBy).AsEnumerable();

			items = GetFilteredItems(items, searchString);
			items = GetSortedItems(items, sortOrder);

			return items;
		}

		public IEnumerable<User> GetUsers()
		{
			var users = _session.Query<User>();
			var items = _session.Query<SomeItem>().ToList();

			users.ForEach(u =>
			              	{
								u.ItemsCreated = items.Where(i => 
									string.Equals(i.CreatedBy.Name, u.Name, StringComparison.InvariantCultureIgnoreCase)).
									ToList();
								u.ItemsUpdated = items.Where(i => 
									string.Equals(i.UpdatedBy.Name, u.Name, StringComparison.InvariantCultureIgnoreCase)).
									ToList();
			              	});

			return users;
		}

		public SomeItem GetItemById(int someId)
		{
			return _session.Query<SomeItem>()
						.Include(si => si.CreatedBy)
						.Include(si => si.UpdatedBy)
						.Single(si => si.Id == someId);
		}

		public void InsertItem(SomeItem someItem, string userName)
		{
			if (string.IsNullOrWhiteSpace(userName)) return;

			var tmpUser = _session.Query<User>().
				Single(user => string.Equals(user.Name, userName, StringComparison.InvariantCultureIgnoreCase));

			someItem.UpdatedBy = tmpUser;
			someItem.CreatedBy = tmpUser;

			_session.Store(someItem);
		}

		public void DeleteItem(int someId)
		{
			var itemToDelete = _session.Load<SomeItem>(someId);
			DeleteItem(itemToDelete);
		}

		public void DeleteItem(SomeItem someItem)
		{
			if (someItem != null)
			{
				_session.Delete(someItem);
			}
		}

		public void UpdateItem(SomeItem someItem)
		{
			_session.Store(someItem, someItem.Id.ToString());
		}

		public User GetUserByName(string userName)
		{
			User user = null;
			if (!string.IsNullOrWhiteSpace(userName))
			{
				user = _session.Query<User>()
					.Single(u => u.Name.ToLower() == userName.ToLower());
			}

			return user;
		}

		public void LoadReference(SomeItem someItem)
		{
			if (someItem != null)
			{
				var tmpItem = _session.Query<SomeItem>()
					.Include(si => si.CreatedBy)
					.Include(si => si.UpdatedBy)
					.Single(si => si.Id == someItem.Id);

				someItem.UpdatedBy = tmpItem.UpdatedBy;
				someItem.CreatedBy = tmpItem.CreatedBy;
			}
		}

		public void Save()
		{
			_session.SaveChanges();
		}

		/// <summary>
		/// Unlock all user's items if they are locked
		/// </summary>
		/// <param name="userName">User name</param>
		public void UnlockAllUsersItems(string userName)
		{
			var user = GetUserByName(userName);
			var lockedItemsList = user.ItemsUpdated.Where(it => it.Locked).ToList();
			lockedItemsList.ForEach(it => it.Locked = false);

			Save();
		}

		#endregion

		#region Implementation of IDisposable

		public void Dispose()
		{
			if (_session != null) _session.Dispose();
		}

		#endregion
	}
}