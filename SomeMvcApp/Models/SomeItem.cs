﻿using System.ComponentModel.DataAnnotations;

namespace SomeMvcApp.Models
{
    public class SomeItem
    {
        public SomeItem()
        {
        }

        public SomeItem(User user)
        {
            CreatedBy = user;
            UpdatedBy = user;
        }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        [MinLength(5, ErrorMessage = "Title cannot be shorter than 5 characters.")]
        public string Title { get; set; }
        public string Description { get; set; }
        [Timestamp]
        public byte[] TimeStamp { get; set; }
        [Display(Name = "Created by")]
        public User CreatedBy { get; set; }
        [Display(Name = "Updated by")]
        public User UpdatedBy { get; set; }
        public bool Locked { get; set; }
    }
}