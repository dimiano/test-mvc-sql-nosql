﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SomeMvcApp.Models
{
    public class User
    {
        public User()
        {
            Created = DateTime.UtcNow;
        }

        public User(string name)
            :this()
        {
            Name = name;
        }

        [Key]
        public int Id { get; set; }
        [Required]
        //EF doesn't support Unique Constraint
        [Display(Name = "User name")]
        [MaxLength(100, ErrorMessage = "User Name must be 100 characters or less"), MinLength(3)]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Created date")]
        public DateTime Created { get; set; }
        [InverseProperty("CreatedBy")]
        [Display(Name = "Items created")]
        public virtual IList<SomeItem> ItemsCreated { get; set; }
        [InverseProperty("UpdatedBy")]
        [Display(Name = "Items updated")]
        public virtual IList<SomeItem> ItemsUpdated { get; set; }
    }
}
