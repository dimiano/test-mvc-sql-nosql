﻿using System.Web.Mvc;

namespace SomeMvcApp.Controllers
{
    public class HomeController : Controller
    {
        private const string SomeTitle = "Some ASP.NET MVC application.";

        public ActionResult Index()
        {
            ViewBag.Message = SomeTitle;

            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Some");
            }

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = SomeTitle;

			if (User.Identity.IsAuthenticated)
			{
				return RedirectToAction("About", "Some");
			}

            return View();
        }
    }
}
