﻿using System;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net.Http;
using System.Web.Mvc;
using System.Web.Routing;
using PagedList;
using Raven.Client.Embedded;
using SomeMvcApp.Models;
using SomeMvcApp.Models.Repository;

namespace SomeMvcApp.Controllers
{
	public class SomeController : Controller
	{
		private readonly IRepository _repository;

		public SomeController()
		{
			//_repository = new SomeContextRepository(new SomeContext());
			_repository = new SomeDocRepository(new EmbeddableDocumentStore { ConnectionStringName = "RavenEmbedded" });
		}
		
		//
		// GET: /Some/

		public ActionResult Index(string sortOrder, string searchString, string currentFilter, int pageNumber = 1)
		{
			ViewBag.SortParam = sortOrder;

			if (Request.HttpMethod == HttpMethod.Get.ToString())
			{
				searchString = currentFilter;
			}
			else // reset paging with due to new filter
			{
				pageNumber = 1;
			}
			ViewBag.CurrentFilter = searchString;

			const int pageSize = 10;                                 //https://github.com/martijnboland/MvcPaging/issues/15
			return View(_repository.GetItems(sortOrder, searchString).ToList().ToPagedList(pageNumber, pageSize));
		}
		
		//
		// GET: /Some/Details/5

		public ActionResult Details(int id = 0)
		{
			SomeItem someitem = _repository.GetItemById(id);

			if (someitem == null)
			{
				return HttpNotFound();
			}
			return View(someitem);
		}

		//
		// GET: /Some/Create

		public ActionResult Create()
		{
			return View();
		}

		//
		// POST: /Some/Create

		[HttpPost]
		public ActionResult Create(SomeItem someitem)
		{
			try
			{
				if (ModelState.IsValid)
				{
					_repository.InsertItem(someitem, User.Identity.Name);
					_repository.Save();

					return RedirectToAction("Index");
				}
			}
			catch (DataException)
			{
				//Log the error (add a variable name after DataException)
				ModelState.AddModelError("", "Unable to save changes. Try again, please.");
			}

			return View(someitem);
		}

		//
		// GET: /Some/Edit/5/true

		public ActionResult Edit(int id = 0, bool isLocked = false)
		{
			//bool isLocked = Convert.ToBoolean(ViewBag.IsLocked);
			SomeItem someitem = _repository.GetItemById(id);
			
			if (someitem == null)
			{
				return HttpNotFound();
			}
			
			//currently is not locked but needs lock
			if (!someitem.Locked && isLocked)
			{
				SetItemLock(someitem, true);
			}
			//currently is locked but not needs lock
			else if (someitem.Locked && !isLocked)
			{
				SetItemLock(someitem, false);
			}

			return View(someitem);
		}

		private void SetItemLock(SomeItem someitem, bool isLocked)
		{
			try
			{
				someitem.UpdatedBy = _repository.GetItems().First(si => si.Id == someitem.Id).UpdatedBy;
				var currentUser = _repository.GetUserByName(User.Identity.Name);

				if (someitem.UpdatedBy == currentUser || !someitem.Locked)
				{
					someitem.UpdatedBy = currentUser;
					someitem.Locked = isLocked;
					_repository.UpdateItem(someitem);
					_repository.Save();
			}
		}
			catch (InvalidOperationException)
			{
				/*Sequence contains no elements*/
			}
		}

		//
		// POST: /Some/Edit/5

		[HttpPost]
		public ActionResult Edit(SomeItem someitem)
		{
			try
			{
				if (ModelState.IsValid)
				{
					someitem.UpdatedBy = _repository.GetUserByName(User.Identity.Name);
					_repository.UpdateItem(someitem);
					_repository.Save();

					return RedirectToAction("Index");
				}
			}
			catch (DbUpdateConcurrencyException ex)
			{
				var entry = ex.Entries.Single();
				var clientValues = (SomeItem)entry.Entity;
				//_db.Entry(clientValues).Reference(si => si.UpdatedBy).Load();
				_repository.LoadReference(clientValues);
				var databaseValues = (SomeItem)entry.GetDatabaseValues().ToObject();
				databaseValues.UpdatedBy = _repository.GetItems().First(si => si.Id == databaseValues.Id).UpdatedBy; //_db.SomeItems.Include("UpdatedBy").First(si => si.Id == databaseValues.Id).UpdatedBy;
				
				if (databaseValues.Title != clientValues.Title)
				{
					ModelState.AddModelError("Title", "Current value: " + databaseValues.Title);
				}
				
				if (databaseValues.Description != clientValues.Description)
				{
					ModelState.AddModelError("Description", "Current value: " + databaseValues.Description);
				}

				if (databaseValues.UpdatedBy.Name != clientValues.UpdatedBy.Name)
				{
					ModelState.AddModelError("UpdatedBy", "Current value: " + databaseValues.UpdatedBy.Name);
				}
				ModelState.AddModelError(string.Empty, "The record you attempted to edit "
					+ "was modified by another user after you got the original value. The "
					+ "edit operation was canceled and the current values in the database "
					+ "have been displayed. If you still want to edit this record, click "
					+ "the Save button again. Otherwise click the Cancel to return to list of items.");
				someitem.TimeStamp = databaseValues.TimeStamp;

				///////////////////Permanent Save///////////////////
				//var updatedItem = _db.SomeItems.Find(someitem.Id);
				//var currentUser = _db.Users.Single(u => u.Name.ToLower() == User.Identity.Name.ToLower());
				//updatedItem.UpdatedBy = currentUser;
				//// end editing
				//updatedItem.Locked = false;
				//_db.Entry(updatedItem).CurrentValues.SetValues(someitem);
				//_db.SaveChanges();
			}
			catch (DataException)
			{
				//Log the error
				ModelState.AddModelError(string.Empty, "Unable to save changes. Try again.");
			}

			return View(someitem);
		}
		
		[HttpPost]
		public JsonResult JsonSave(SomeItem someItem)
		{
			bool saved = false;
			string message = string.Empty;

			try
			{
				if (ModelState.IsValid)
				{
					someItem.UpdatedBy = _repository.GetUserByName(User.Identity.Name);
					((SomeContextRepository)_repository).StrongUpdateItem(someItem);
					saved = true;
				}
			}
			catch (Exception ex)
			{
				message = ex.Message;
			}

			return Json(Tuple.Create(saved, message)); //Json(someItem, JsonRequestBehavior.AllowGet);
		}

		public void JsonUnlock(SomeItem someItem)
		{
			_repository.UnlockAllUsersItems(User.Identity.Name);
		}

		//
		// GET: /Some/Delete/5

		public ActionResult Delete(int id = 0, bool concurrencyError = false)
		{
			SomeItem someitem = _repository.GetItemById(id);

			if (someitem == null)
			{
				return HttpNotFound();
			}

			if (concurrencyError)
			{
				ViewBag.ConcurrencyErrorMessage = "The record you attempted to delete "
					+ "was modified by another user after you got the original values. "
					+ "The delete operation was canceled and the current values in the "
					+ "database have been displayed. If you still want to delete this "
					+ "record, click the <b>Delete</b> button again. Otherwise "
					+ "click the <b>Back to List</b> hyperlink.";
			}

			return View(someitem);
		}

		//
		// POST: /Some/Delete/5

		[HttpPost, ActionName("Delete")]
		public ActionResult DeleteConfirmed(SomeItem someItem)
		{
			try
			{
				_repository.DeleteItem(someItem);
				_repository.Save();

				return RedirectToAction("Index");
			}
			catch (DbUpdateConcurrencyException)
			{
				return RedirectToAction("Delete", new RouteValueDictionary { {"concurrencyError", true} });
			}
			catch (DataException)
			{
				//Log the error (add a variable name after Exception) 
				ModelState.AddModelError(string.Empty, "Unable to save changes. Try again, and if the problem persists contact your system administrator.");
				return View(someItem);
			} 
		}

		public ActionResult About()
		{
			return View(_repository.GetUsers());
		}

		protected override void Dispose(bool disposing)
		{
			_repository.Dispose();
			base.Dispose(disposing);
		}

	}
}